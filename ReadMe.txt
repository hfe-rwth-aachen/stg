Smith-Chart Task Generator Version 1.0 17/05/2023

Usage notes
-------------------------------------------------
- This application 
calculates the input impedance and the input 
reflection coefficient of a circuit given by the 
user 
or 
randomly generates a circuit task based on the 
users input parametersto be solved in the Smith 
Chart.
- The type of task has to be chosen in the bottom
left drop-down menu. 
- The "Show Solution" button toggles the 
visibility of the solution to the task.
- The "Generate Task" button appears when 
"Generate Random Task" is chosen as the type of 
task. At each click a new task is randomly 
generated.
- Please note that in rare cases, in which the 
load to line one is a short circuit, the randomly 
generated task might not be, as 
the solution might suggest, uniquely solvable.
-------------------------------------------------

Installation notes
-------------------------------------------------
- This application does not require installation.
- The index.html and the Pictures Folder should 
be kept in the same directory.
- It should then be possible to run the 
application with any webbrowser by opening the 
index.html file. 
-------------------------------------------------

Contact
-------------------------------------------------
Website: hfe.rwth-aachen.de
E-Mail: contact@hfe.rwth-aachen.de

Copyright
-------------------------------------------------
Copyright 2023 
Chair of High Frequency Electronics
RWTH Aachen University

Permission is hereby granted, free of charge, to
any person obtaining a copy of this software and 
associated documentation files (the “Software”), 
to deal in the Software without restriction, 
including without limitation the rights to use, 
copy, modify, merge, publish, distribute, 
sublicense, and/or sell copies of the Software, 
and to permit persons to whom the Software is 
furnished to do so, subject to the following 
conditions:
The above copyright notice and this permission 
notice shall be included in all copies or 
substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT 
WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF 
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
 AND NONINFRINGEMENT. IN NO EVENT SHALL THE 
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY 
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN 
ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


